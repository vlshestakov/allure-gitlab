import java.lang.Thread.sleep

group = "io.eroshenkoam"
version = version

plugins {
    java
    maven
    id("io.qameta.allure") version "2.8.1"
}

allure {
    autoconfigure = true
    aspectjweaver = true
    version = "2.13.8"

    useJUnit5 {
        version = "2.13.8"
    }

    resultsDir = project.file("allure-results")

}

tasks.withType(JavaCompile::class) {
    sourceCompatibility = "${JavaVersion.VERSION_1_8}"
    targetCompatibility = "${JavaVersion.VERSION_1_8}"
    options.encoding = "UTF-8"
}

tasks.test {
    ignoreFailures = true
    useJUnitPlatform {
        includeEngines("junit-jupiter")
    }
    systemProperty("junit.jupiter.execution.parallel.enabled", "true")
    systemProperty("junit.jupiter.execution.parallel.config.strategy", "dynamic")

    systemProperty("junit.jupiter.extensions.autodetection.enabled", "true")
    doLast {
        sleep(10 * 1000)
    }
}


repositories {
    maven(url = "https://dl.bintray.com/qameta/maven-unstable/")
    mavenCentral()
    mavenLocal()
}

dependencies {
    compile("commons-io:commons-io:2.6")
    compile("io.qameta.allure:allure-java-commons:2.13.8")
    compile("org.junit.jupiter:junit-jupiter-api:5.7.1")
    compile("org.junit.jupiter:junit-jupiter-engine:5.7.1")
    compile("org.junit.jupiter:junit-jupiter-params:5.7.1")

//    testCompile("io.qameta.allure:allure-ee-junit-platform:3.28.2")
}
